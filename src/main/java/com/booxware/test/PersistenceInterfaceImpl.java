package com.booxware.test;

import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

public class PersistenceInterfaceImpl implements PersistenceInterface {

    private final Map<String, Account> storage = new HashMap<>();

    @Override
    public void save(@NonNull Account account) {
        storage.put(account.getUsername(), account);
    }

    @Override
    public Account findById(long id) {
        return storage
                .values()
                .stream()
                .filter(account -> id == account.getId())
                .findFirst()
                .orElse(null);
    }

    @Override
    public Account findByName(@NonNull String name) {
        return storage.get(name);
    }

    @Override
    public void delete(@NonNull Account account) {
        storage.remove(account.getUsername());
    }
}
