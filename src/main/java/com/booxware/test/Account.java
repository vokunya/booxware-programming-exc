package com.booxware.test;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;

import java.io.Serializable;
import java.util.Date;

/**
 * The encryption can be very simple, we don't put much emphasis on the
 * encryption algorithm.
 */

@Builder
@AllArgsConstructor
@Getter
public class Account implements Serializable {

    private long id;

    private String username;

    private byte[] encryptedPassword;

    private String salt;

    private String email;

    @Wither
    private Date lastLogin;

}
