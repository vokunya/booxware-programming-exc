package com.booxware.test;

import com.booxware.test.encryption.PasswordService;
import lombok.NonNull;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;

public class AccountServiceInterfaceImpl implements AccountServiceInterface {

    private final PersistenceInterface persistenceService;
    private final PasswordService passwordService;

    public AccountServiceInterfaceImpl(PersistenceInterface persistenceService, PasswordService passwordService) {
        this.persistenceService = persistenceService;
        this.passwordService = passwordService;
    }

    @Override
    public Account login(@NonNull String username, @NonNull String password) {
        return Optional.ofNullable(persistenceService.findByName(username))
                .map(account -> updateLastLoginIfPasswordCorrect(account, password))
                .map(account -> {
                    persistenceService.save(account);
                    return account;
                })
                .orElseThrow(() -> new AccountServiceException("User '" + username + "' does not exist"));
    }

    @Override
    public Account register(@NonNull String username, @NonNull String email, @NonNull String password) {
        Account account = persistenceService.findByName(username);
        if (account == null) {
            String salt = passwordService.generateSalt();
            String passwordHash = passwordService.hashPassword(password, salt);

            account = Account.builder()
                    .username(username)
                    .email(email)
                    .encryptedPassword(passwordHash.getBytes(StandardCharsets.UTF_8))
                    .salt(salt)
                    .build();

            persistenceService.save(account);

            return login(username, password);
        } else {
            throw new AccountServiceException("Account '" + username + "' already exists");
        }
    }

    @Override
    public void deleteAccount(@NonNull String username) {
        Optional.ofNullable(persistenceService.findByName(username))
                .map(account -> {
                    persistenceService.delete(account);
                    return account;
                })
                .orElseThrow(() -> new AccountServiceException("User '" + username + "' does not exist"));
    }

    @Override
    public boolean hasLoggedInSince(@NonNull String username, @NonNull Date since) {
        Account account = Optional
                .ofNullable(persistenceService.findByName(username))
                .orElseThrow(() -> new AccountServiceException("User '" + username + "' does not exist"));

        return lastLoginIsNewerWhenSince(since, account.getLastLogin());
    }

    private Account updateLastLoginIfPasswordCorrect(Account account, String password) {
        if (!passwordCorrect(password, account.getEncryptedPassword())) {
            throw new AccountServiceException("Password is incorrect");
        }
        return updateLastLoginDate(account);
    }

    private boolean passwordCorrect(String password, byte hash[]) {
        return passwordService.checkPassword(password, hash);
    }

    private Account updateLastLoginDate(Account account) {
        Date now = new Date();
        return account.withLastLogin(now);
    }

    private boolean lastLoginIsNewerWhenSince(Date since, Date lastLogin) {
        return lastLogin.compareTo(since) > 0;
    }
}
