package com.booxware.test.encryption;

import org.mindrot.jbcrypt.BCrypt;

import java.nio.charset.StandardCharsets;

public class PasswordService {

    private static int WORKLOAD = 12;

    public String hashPassword(String rawPassword, String salt) {
        return BCrypt.hashpw(rawPassword, salt);
    }

    public String generateSalt() {
        return BCrypt.gensalt(WORKLOAD);
    }

    public boolean checkPassword(String rawPassword, byte[] passwordHash) {
        String hashString = new String(passwordHash, StandardCharsets.UTF_8);
        if (!hashString.startsWith("$2a$"))
            throw new IllegalArgumentException("Invalid hash provided for comparison");

        return BCrypt.checkpw(rawPassword, hashString);
    }
}
