package com.booxware.test.encryption;

import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


public class PasswordServiceTest {

    private PasswordService passwordService;
    private String salt;

    @Before
    public void setUp() {
        passwordService = new PasswordService();
        salt = passwordService.generateSalt();
    }

    @Test
    public void shouldMatchPassword() throws Exception {
        String hashedPassword = passwordService.hashPassword("password", salt);

        boolean isPasswordMatch = passwordService.checkPassword("password", hashedPassword.getBytes(StandardCharsets.UTF_8));

        assertThat(isPasswordMatch).isTrue();
    }

    @Test
    public void shouldNotMatchPassword() throws Exception {
        String hashedPassword = passwordService.hashPassword("password1", salt);

        boolean isPasswordMatch = passwordService.checkPassword("password2", hashedPassword.getBytes(StandardCharsets.UTF_8));

        assertThat(isPasswordMatch).isFalse();
    }

    @Test
    public void shouldThrowExceptionIfSaltIsInvalid() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> passwordService.checkPassword("password", "hash".getBytes(StandardCharsets.UTF_8)))
                .withMessage("Invalid hash provided for comparison")
                .withNoCause();
    }

}