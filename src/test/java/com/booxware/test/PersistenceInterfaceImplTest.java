package com.booxware.test;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PersistenceInterfaceImplTest {

    private PersistenceInterface persistenceService;
    private Account account;

    @Before
    public void setUp() {
        account = Account.builder()
                .username("username")
                .email("email")
                .id(1)
                .build();

        persistenceService = new PersistenceInterfaceImpl();
        persistenceService.save(account);
    }

    @Test
    public void save() throws Exception {
        assertThat(persistenceService.findByName("name")).isNull();

        Account accountToSave = Account.builder().username("name").build();
        persistenceService.save(accountToSave);

        assertThat(persistenceService.findByName("name")).isEqualToComparingFieldByField(accountToSave);
    }

    @Test
    public void findById() throws Exception {
        assertThat(persistenceService.findById(1)).isEqualToComparingFieldByField(account);
    }

    @Test
    public void findByName() throws Exception {
        assertThat(persistenceService.findByName("username")).isEqualToComparingFieldByField(account);
    }

    @Test
    public void delete() throws Exception {
        assertThat(persistenceService.findByName("username")).isNotNull();

        persistenceService.delete(account);

        assertThat(persistenceService.findByName("username")).isNull();
    }

}