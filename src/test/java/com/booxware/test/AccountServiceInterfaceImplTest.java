package com.booxware.test;

import com.booxware.test.encryption.PasswordService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.JANUARY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceInterfaceImplTest {

    private Account account;

    @Mock
    private PersistenceInterface persistenceService;

    @Mock
    private PasswordService passwordService;

    @InjectMocks
    private AccountServiceInterfaceImpl accountService;

    @Before
    public void setUp() {
        this.account = new Account(1, "name", new byte[1], "salt", "email", new Date());
    }

    @Test
    public void shouldLoginIfAccountExistsAndPasswordIsCorrect() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(account);
        when(passwordService.checkPassword(eq("password"), any())).thenReturn(true);

        Account updatedAccount = accountService.login("name", "password");

        assertThat(updatedAccount.getLastLogin()).isNotNull();
    }

    @Test
    public void shouldThrowExceptionIfAccountDoesNotExist() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(null);

        assertThatExceptionOfType(AccountServiceException.class)
                .isThrownBy(() -> accountService.login("name", "password"))
                .withMessage("User 'name' does not exist")
                .withNoCause();
    }

    @Test
    public void shouldThrowExceptionIfPasswordIsIncorrect() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(account);
        when(passwordService.checkPassword(eq("password"), any())).thenReturn(false);

        assertThatExceptionOfType(AccountServiceException.class)
                .isThrownBy(() -> accountService.login("name", "password"))
                .withMessage("Password is incorrect")
                .withNoCause();
    }

    @Test
    public void shouldRegisterIfUserDoesNotExist() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(null).thenReturn(account);
        when(passwordService.generateSalt()).thenReturn("salt");
        when(passwordService.hashPassword(eq("password"), eq("salt"))).thenReturn("hashed_password");
        when(passwordService.checkPassword(eq("password"), any())).thenReturn(true);

        Account account = accountService.register("name", "email", "password");

        verify(persistenceService, times(2)).save(any());
        assertThat(account.getLastLogin()).isNotNull();
        assertThat(account.getEmail()).isEqualTo("email");
        assertThat(account.getSalt()).isEqualTo("salt");
        assertThat(account.getUsername()).isEqualTo("name");
    }

    @Test
    public void shouldThrowExceptionIfRegisteredUserAlreadyExist() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(account);

        assertThatExceptionOfType(AccountServiceException.class)
                .isThrownBy(() -> accountService.register("name", "email", "password"))
                .withMessage("Account 'name' already exists")
                .withNoCause();
    }

    @Test
    public void shouldDeleteAccountIfExists() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(account);

        accountService.deleteAccount("name");

        verify(persistenceService, times(1)).delete(account);
    }

    @Test
    public void shouldThrowExceptionIfDeleteAccountWhichNotExist() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(null);

        assertThatExceptionOfType(AccountServiceException.class)
                .isThrownBy(() -> accountService.deleteAccount("name"))
                .withMessage("User 'name' does not exist")
                .withNoCause();
        verify(persistenceService, never()).delete(account);
    }

    @Test
    public void shouldReturnTrueIfUserHasLoggedInSince() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(account);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, JANUARY, 1);
        Date since = calendar.getTime();

        assertThat(accountService.hasLoggedInSince("name", since)).isTrue();
    }

    @Test
    public void shouldThrowExceptionIfUserNotExistWhileCheckingLastLogin() throws Exception {
        when(persistenceService.findByName("name")).thenReturn(null);

        assertThatExceptionOfType(AccountServiceException.class)
                .isThrownBy(() -> accountService.hasLoggedInSince("name", new Date()))
                .withMessage("User 'name' does not exist")
                .withNoCause();
    }

}